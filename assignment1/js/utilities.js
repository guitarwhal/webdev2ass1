"use strict";

/**
 * changes the text color of a passed element
 * @param {*} elem - the element to affect
 * @param {*} color - the color to change the text to
 */
function changeTextColor(elem, color){
    elem.style.color = color;
}

/**
 * changes the background color of a passed element
 * @param {*} elem - the element to affect
 * @param {*} color - the color to change the background to
 */
function changeBackground(elem, color){
    elem.style.backgroundColor = color;
}

/**
 * changes the width of an element by a passed percent
 * @param {*} elem - the element to affect
 * @param {*} width - the width to change the element to in percent
 */
function changeTagWidth(elem, width){
    elem.width = `${width}%`;
}

/**
 * changes the border color of a passed element
 * @param {*} elem - the element to affect
 * @param {*} color - the color to change the border to
 */
function changeBorderColor(elem, color){
    elem.style.borderColor = color;
}

/**
 * changes the border width of a passed element
 * @param {*} elem - the element to affect
 * @param {*} width - the width to change the border to in pixels
 */
function changeBorderWidth(elem, width){
    elem.border = `${width}px`;
}