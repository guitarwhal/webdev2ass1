"use strict";
document.addEventListener("DOMContentLoaded", setup);

//sets up the form and textarea to update when the form fields are changed
function setup(){
    const form = document.querySelector("form");
    const textarea = document.querySelector("textarea");

    form.addEventListener('change',generateTable());
    textarea.addEventListener('change', sizeTextarea(textarea));
}

//generates or regenerates the table to fit the form inputs
function generateTable(){
    var tblSection = document.getElementById("table-render-space");
    var rowCount = document.querySelector("#row-count").value;
    var colCount = document.querySelector("#col-count").value;
    var tblWidth = document.querySelector("#table-width").value;
    var bgColor = document.querySelector("#background-color").value;
    var textColor = document.querySelector("#text-color").value;
    var borderWidth = document.querySelector("#border-width").value;
    var borderColor = document.querySelector("#border-color").value;
    const textarea = document.querySelector("textarea");
    var table = document.createElement('table');
    var tbody = document.createElement('tbody');

    tblSection.innerHTML = "";
    table.appendChild(tbody);

    //rows
    for (var i = 0; i < rowCount; i++){
        var tr = document.createElement('tr');

        tbody.appendChild(tr);

        //columns
        for (var j = 0; j < colCount; j++){
            var td = document.createElement('td');

            td.appendChild(document.createTextNode(`Cell${i}${j}`));
            tr.appendChild(td);

            changeTextColor(td, textColor);
        }
    }
    changeBorderWidth(table, borderWidth);
    changeBorderColor(table, borderColor);
    changeBackground(table, bgColor);
    changeTagWidth(table, tblWidth);

    tblSection.appendChild(table);
    textarea.innerHTML = generateHTML(rowCount, colCount);
}

/**
 * generates the html code in text for a table with the passed number of rows and columns
 * @param {*} rowCount - number of rows
 * @param {*} colCount - number of columns
 * @returns 
 */
function generateHTML(rowCount, colCount){
    var text = "<table>\n";

    for(var i = 0; i < rowCount; i++){
        text += "   <tr>\n";

        for(var j = 0; j < colCount; j++){
            text += `       <td>Cell${i}${j}</td>\n`;
        }
        text += "   </tr>\n";
    }
    text += "</table>";

    return text;
}

/**
 * resizes a passed textarea to be the height of the scroll bar
 * @param {*} textarea - the textarea to resize
 */
function sizeTextarea(textarea){
    textarea.style.height = `${textarea.scrollHeight}px`;
}